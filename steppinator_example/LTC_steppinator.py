import os
import re
import sqlite3
from uuid import uuid4
import subprocess
import time
from PyLTSpice import RawRead
from IPython.utils import io

def run_id(run_id):
    #check if run is already completed:
    if run_id+".raw" in os.listdir("./"):
        print("The run id "+run_id+" has already been completed\n")
    else:
        cmd = 'wine C:\\\\Program\\ Files\\\\LTC\\\\LTspiceXVII\\\\XVIIx64.exe -Run -b ' + "runs/"+run_id + ".asc &> /dev/null"
            
    subprocess.run(cmd, shell=True)
    
def to_fastaccess(run_id):
    cmd = 'wine C:\\\\Program\\ Files\\\\LTC\\\\LTspiceXVII\\\\XVIIx64.exe -FastAccess ' + "runs/"+run_id + ".raw &> /dev/null"
    subprocess.run(cmd, shell=True)

class LTC_steppinator:
    
    def __init__(self, sim_file='', ltspice_command=''):
        self.sim_file = sim_file
        
        with open(self.sim_file, "rb") as f:
            self.sim_file_content = f.read()
        
        self.param_names = self.read_param_names_from_asc()
        self.named_net_names = self.read_named_nets_from_asc()
        
        if ltspice_command:
            self.ltspice_command = ltspice_command
        else:
            self.ltspice_command = 'wine C:\\\\Program\\ Files\\\\LTC\\\\LTspiceXVII\\\\XVIIx64.exe -Run -b '
            
        self.mode = "fresh"
        self.setup_run_db()
        if not os.path.exists('./runs'):
            os.mkdir('./runs')
            
    def __del__(self):
        self.con.commit()
        self.con.close()
            
    def read_param_names_from_asc(self):
        """
        Read parameter names from simulation file

        Returns
        -------
        list : list of all parameter names

        """
        param_names = []
        
        param_name_re = re.compile(r"{[A-Za-z0-9_]*}")
    
        param_names = param_name_re.findall(str(self.sim_file_content))
        return list(set(map(lambda n: n[1:-1], param_names)))

    def read_named_nets_from_asc(self):
        named_nets_names = []
        named_nets_name_re = re.compile(r"FLAG[0-9 ]*[a-zA-Z_]+")
        named_nets_names = named_nets_name_re.findall(str(self.sim_file_content))
        named_nets_names = list(map(lambda t: t.split()[-1], named_nets_names))
        return named_nets_names

    def setup_run_db(self):
        """
        Create a new database if none exists and populate the runs table

        """
        if os.path.exists(self.sim_file[:-4]+"_runs.db"):
            self.mode = "reload"
        self.con = sqlite3.connect(self.sim_file[:-4]+"_runs.db")
        self.cur = self.con.cursor()
        
        if self.mode == "fresh":
            self.cur.execute("""CREATE TABLE runs (run_id TEXT PRIMARY KEY, sim_done TEXT, convert_done TEXT)""")
            self.cur.execute("""CREATE TABLE evals (run_id TEXT PRIMARY KEY, interp_done TEXT)""")
                
            for p in self.param_names:
                self.cur.execute(f"""ALTER TABLE runs ADD COLUMN {p} FLOAT;""")
            
        self.con.commit()

    
    def add_run_to_db(self, params):
        """
        Add a run (set of parameter values) to the database

        Parameters
        ----------
        params : dict
            dictionary with parameter values for this run

        Raises
        ------
        LookupError
            supplying parameters with names not in the runs database raise a LookupError

        """
        params_complete = dict(params)

        params_complete["run_id"] = uuid4().hex
        params_complete["sim_done"] = ""
        params_complete["convert_done"] = ""

        #do keys of params and database match?
        table_info = self.cur.execute("""PRAGMA table_info(runs);""")
        col_names_db = [el[1] for el in table_info.fetchall()]
        for k in params.keys():
            if k not in col_names_db:
                raise LookupError("param name "+str(k)+" is not a valid column name in the runs database")
            
        #has a run with given parameters already been compiled?
        db_string = "SELECT * FROM runs WHERE "
        for k in params.keys():
            db_string = db_string + str(k) + " = "+str(params[k]) + " AND "
        hits = self.cur.execute(db_string[:-5]) 
            
        if hits.fetchall():
            print("This parameter combination already exists: "+str(params))
            return
        else:
            print("Insert "+str(params))
    
        #insert run parameters
        db_string_1 = "INSERT INTO runs (run_id, sim_done, convert_done, "
        db_string_2 = "VALUES(:run_id, :sim_done, :convert_done, "

        for n in params.keys():
            db_string_1 = db_string_1 + str(n) + ", "
            db_string_2 = db_string_2 + ":" + str(n) + ", "
        db_string_1 = db_string_1[:-2] +")"
        db_string_2 = db_string_2[:-2] +")"

        db_string = db_string_1 + " " + db_string_2

        self.con.executemany(db_string, (params_complete,))

        self.con.commit()
        

        evals_params = {k:None for k in self.get_eval_results_names()}
        evals_params["run_id"] = params_complete["run_id"]
        evals_params["interp_done"] = ''
        db_string = "INSERT INTO evals VALUES("
        for k in evals_params.keys():
            db_string = db_string + ":"+k+", "
        db_string = db_string[:-2] +")" 
        self.con.executemany(db_string, (evals_params,))
    
        self.con.commit()
        
        self.generate_run_asc(params_complete["run_id"])
        self.write_param_file(params_complete["run_id"], params)

    def get_params(self, run_id):

        table_info = self.cur.execute("""PRAGMA table_info(runs);""")
        col_names_db = [el[1] for el in table_info.fetchall()]

        res = self.cur.execute("SELECT * FROM runs WHERE run_id=\""+run_id+"\"")
        param_vals = [el for el in res.fetchall()[0]]

        d = dict(zip(col_names_db, param_vals))

        return {key: d[key] for key in self.param_names}


    def get_ids_where(self, params):
        relevant_params = {key: params[key] for key in params if not params[key]==None}
        db_string = "SELECT * FROM runs WHERE "
        for k in relevant_params.keys():
            db_string = db_string + k + "="+ str(relevant_params[k])+" AND "
        res = self.cur.execute(db_string[:-5])
        return [el[0] for el in res.fetchall()]


    def add_eval_results(self, eval_name, results):
        #Find which eval runs are available
        available_evals = self.get_eval_results_names()
        
        #if eval name is not in available runs, create new column and insert the results
        if eval_name not in available_evals:
            self.cur.execute(f"""ALTER TABLE evals ADD COLUMN {eval_name} FLOAT;""")  
        for k in results.keys():
            val = results[k]
            db_string = f"""UPDATE evals SET {eval_name} = {val} WHERE run_id=\"{k}\""""
            self.con.execute(db_string)
        self.con.commit()
        
    def get_eval_results_names(self):
        table_info = self.cur.execute("""PRAGMA table_info(evals);""")
        col_names_db = [el[1] for el in table_info.fetchall()]
        return col_names_db

    def get_eval_result(self, run_id, eval_name):
        res = self.cur.execute("SELECT "+eval_name+" FROM evals WHERE run_id=\""+run_id+"\"")
        return res.fetchall()[0]
    
    def get_sim_todo_list(self):
        db_string = "SELECT run_id FROM runs WHERE sim_done=''"
        hits = self.cur.execute(db_string) 
        todo = [el[0] for el in hits.fetchall()]
        return todo
    
    def get_convert_todo_list(self):
        db_string = "SELECT run_id FROM runs WHERE convert_done='compressedraw'"
        hits = self.cur.execute(db_string) 
        todo = [el[0] for el in hits.fetchall()]
        return todo
    
    def get_all_run_ids(self):
        db_string = "SELECT run_id FROM runs"
        hits = self.cur.execute(db_string) 
        todo = [el[0] for el in hits.fetchall()]
        return todo
    
    def get_param_range(self, param_name):
        db_string = "SELECT "+param_name+" FROM runs"
        hits = self.cur.execute(db_string) 
        param_range = [el[0] for el in hits.fetchall()]
        return sorted(list(set(param_range)))
    
    def update_status_lists(self, selection):
        if selection:
            ids = selection
        else:
            ids = self.get_all_run_ids()#
        for run_id in ids:
            if run_id+".raw" in os.listdir("./runs/"):
                with io.capture_output() as captured:
                    raw = RawRead("runs/"+run_id+".raw");
                format_flag = raw.get_raw_property('Flags').split()[-1]
                date = raw.get_raw_property('Date')
                self.cur.execute("UPDATE runs SET sim_done = \"" + str(date)+"\" WHERE run_id = \""+run_id + "\"")
                self.cur.execute("UPDATE runs SET convert_done = \"" + str(format_flag)+"\" WHERE run_id = \""+run_id + "\"")
            if run_id+".pkl" in os.listdir("./runs/"):
                print("Updating eval for "+run_id)
                self.cur.execute("UPDATE evals SET interp_done = \"" + str(os.path.getmtime("./runs/"+run_id+".pkl"))+"\" WHERE run_id = \""+run_id + "\"")
        self.con.commit()
                
    def cleanup(self, run_id):
        files = os.listdir("./runs/")
        if run_id+".raw" in files:
            os.remove("runs/"+run_id+".raw")
            os.remove("runs/"+run_id+".net")
            os.remove("runs/"+run_id+"_params.txt")
            os.remove("runs/"+run_id+".log")
            os.remove("runs/"+run_id+".asc")
        if run_id+".op.raw" in files:
            os.remove("runs/"+run_id+".op.raw")

        self.cur.execute("UPDATE runs SET convert_done = \"" + str("raw_deleted")+"\" WHERE run_id = \""+run_id + "\"")
        self.con.commit()
        
    def generate_run_asc(self, run_id):
            
        add_string = "\nTEXT 0 0 Left 2 !.include "+run_id+"_params.txt"
        augmented_contents = self.sim_file_content + bytes(add_string, "ascii")
    
        with open("./runs/"+run_id+".asc", "wb") as f:
            f.write(augmented_contents)
        
    def write_param_file(self, run_id, params):
        with open("./runs/"+run_id+"_params.txt", "w") as f:
            for n in params.keys():
                f.write(".param "+n+" "+"{:.5E}".format(params[n])+"\n")

            
    
    
    