#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#%%
import re
import os
import numpy as np
import pickle

from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import functools

import matplotlib as mpl
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [20, 14]

from scipy.interpolate import interp1d
from PyLTSpice import RawRead

from LTC_steppinator import LTC_steppinator, run_id

#%%
#functions to find all parameters and named nets in an .asc file

def find_parameter_names(sim_file_contents):
    param_name_re = re.compile(r"{[A-Za-z0-9_]*}")
    param_names = param_name_re.findall(str(sim_file_contents))
    return list(sorted(set(map(lambda n: n[1:-1], param_names))))

def find_net_names(sim_file_contents):
    net_name_re = re.compile(r"FLAG[0-9 ]*[a-zA-Z_]+")
    net_names = net_name_re.findall(str(sim_file_contents))
    return list(sorted(map(lambda t: t.split()[-1], net_names)))

#%%
with open("hf_detector.asc", "rb") as f:
    file = str(f.read())

print("Parameters: "+str(find_parameter_names(file)))
print("Nets: "+str(find_net_names(file)))

#%%
#function to convert power in dBm to V into Z Ohms

def dBm_to_V(val, Z=50, mode="RMS"):
    if mode == "PK":
        return np.sqrt(2)*np.sqrt(((10**(val/10))*1e-3)*Z)
    else:
        return np.sqrt(((10**(val/10))*1e-3)*Z)
    
#%%
input_levels_dBm = np.arange(-30, 10, 1)   
input_levels_V = dBm_to_V(input_levels_dBm, Z=50, mode="PK")

prettyprint = lambda x: float("{:2.3f}".format(x))
print("Input levels in dBm: "+str([prettyprint(x) for x in input_levels_dBm]))
print("Input levels in V_pk: "+str([prettyprint(x) for x in input_levels_V]))

#%%
#create a folder for the plots
if "pictures" not in os.listdir("./"):
    os.mkdir("pictures")
    
#%%
#from here on, i'll use the steppinator

steppi = LTC_steppinator("hf_detector.asc")

#%%
#add initial runs to the database. Input voltage sweep at three temperatures

params = {n:0 for n in steppi.param_names}

params["freq"] = 14e6
params["v_offset"] = 0.0
params["r_load"] = 10e3

input_levels_dBm = np.arange(-30, 10, 1)   
input_levels_V = dBm_to_V(input_levels_dBm, Z=50, mode="PK")

temperatures = [10.0, 25.0, 40.0]

for l in input_levels_V:
    for t in temperatures:
        params["temp"] = t
        params["u_hf"] = l
        
        steppi.add_run_to_db(params)
        
#%%
#define a function to interpolate selected traces from a ltspice raw file, and save them to a pickle file

def interpolate(run_id, traces, dt, save=True):
    raw = RawRead("runs/"+run_id+".raw")
    data = dict()
    
    data["time"] = raw.get_trace("time")
    new_time = np.arange(data["time"][0], data["time"][-1], dt)
    
    interp_data = dict()
    interp_data["dt"] = dt
    
    for t in traces:
        data[t] = raw.get_trace(t)
        trace_inter = interp1d(data["time"], data[t])
        interp_data[t] = trace_inter(new_time)

    if save:
        with open("runs/"+run_id+'.pkl', 'wb') as f:
            pickle.dump(interp_data, f)
    else:
        return interp_data
    
#%%
#run all simulations, interpolate the results (see below for timestep discussion), uppdate database and clean up
def run_all_sims():
    to_do_list = steppi.get_sim_todo_list()
    print("Running Simulations: "+str(to_do_list))
    num_workers = 28 #adjust this if necessary
    chunk_size = 2*num_workers
    chunks = [to_do_list[x:x+chunk_size] for x in range(0, len(to_do_list), chunk_size)]

    for chunk in chunks:    
        #Simulate
        print("Chunk: "+str(chunk))
        with ProcessPoolExecutor(max_workers=num_workers) as sim_executor:
            sim_executor.map(run_id, chunk)
            #sim_executor.shutdown(wait=True)
        
        #Interpolate and save *.pkl
        with ProcessPoolExecutor(max_workers=num_workers) as post_executor:
            ret = post_executor.map(functools.partial(  interpolate, 
                                                        traces=["V(v_in)", 
                                                                "V(v_offset)", 
                                                                "V(v_out)"], 
                                                        dt = 10e-9), 
                                                        chunk)
            #post_executor.shutdown(wait=True)

        #Update the db which ids have been run
        steppi.update_status_lists(chunk)

        #delete the .raw files
        for r in chunk:
            steppi.cleanup(r)
            
run_all_sims()

#%%
#function to compute dc values fron the pulsed output

def extract_dc_metrics(run_id):
    with open("runs/"+run_id+'.pkl', 'rb') as f:
        data = pickle.load(f) 

    dt = data["dt"]
    interval_high = {"start": int(80e-6/dt), "stop": int(100e-6/dt)}
    interval_low = {"start": int(180e-6/dt), "stop": int(200e-6/dt)}
    
    v_out_pwr = np.average(data["V(v_out)"][interval_high["start"]:interval_high["stop"]])
    v_out_term = np.average(data["V(v_out)"][interval_low["start"]:interval_low["stop"]])
    
    return v_out_pwr, v_out_term

#%%
#compute dc values and add to database

def run_all_evals():
    res_pwr = dict()
    res_term = dict()
    run_ids = steppi.get_all_run_ids()

    for i in run_ids:
        res_pwr[i], res_term[i] = extract_dc_metrics(i)
    
    steppi.add_eval_results("v_out_term", res_term)
    steppi.add_eval_results("v_out_pwr", res_pwr)
    
run_all_evals()

#%%
#gather data from db according to various criteria

select_params = {n:None for n in steppi.param_names}

temp_range = steppi.get_param_range("temp")
offset_range = steppi.get_param_range("v_offset")
u_hf_range = steppi.get_param_range("u_hf")
r_load_range = steppi.get_param_range("r_load")

res = []
select_params["temp"] = 10
select_params["r_load"] = 10e3
for u in u_hf_range:
    select_params["u_hf"] = u
    run_id = steppi.get_ids_where(select_params)
    if run_id:
        res.append(steppi.get_eval_result(run_id[0], "v_out_pwr"))
    
res2 = []
select_params["temp"] = 25
select_params["v_offset"] = 0.0
select_params["r_load"] = 10e3
for u in u_hf_range:
    select_params["u_hf"] = u
    run_id = steppi.get_ids_where(select_params)
    if run_id:
        res2.append(steppi.get_eval_result(run_id[0], "v_out_pwr"))
        
res3 = []
select_params["temp"] = 40
select_params["v_offset"] = 0.0
for u in u_hf_range:
    select_params["u_hf"] = u
    run_id = steppi.get_ids_where(select_params)
    if run_id:
        res3.append(steppi.get_eval_result(run_id[0], "v_out_pwr"))
        
#%%
#build a plot of output voltage over input voltage/power

plt.rcParams['figure.figsize'] = [12, 5]
fig, ax = plt.subplots(1,2)

ax[0].plot(u_hf_range, res, marker='.', color="tab:blue", label="T=10°C")
ax[0].plot(u_hf_range, res2, marker='.', color="grey", label="T=25°C")
ax[0].plot(u_hf_range, res3, marker='.', color="tab:red", label="T=40°C")

ax[1].semilogy(input_levels_dBm, res, marker='.', color="tab:blue")
ax[1].semilogy(input_levels_dBm, res2, marker='.', color="grey")
ax[1].semilogy(input_levels_dBm, res3, marker='.', color="tab:red")

ax[0].set_xlabel("Input Voltage [V]")
ax[1].set_xlabel("Input Power [dBm]")

ax[0].set_ylabel("Output Voltage [V]")
ax[1].set_ylabel("Output Voltage [V]")

ax[0].legend(loc="upper left")

fig.tight_layout(pad=1.15)
#plt.show()
plt.savefig('pictures/basic_vout_curves.png', transparent=False, dpi=100)
plt.close()

#%%
#add more runs with varying offset voltage

params = {n:0 for n in steppi.param_names}

params["freq"] = 14e6
params["temp"] = 25.0
params["r_load"] = 10e3

input_levels_dBm = np.arange(-30, 10, 1)   
input_levels_V = dBm_to_V(input_levels_dBm, Z=50, mode="PK")

offsets = [0.05, 0.1]

for l in input_levels_V:
    for o in offsets:
        params["u_hf"] = l
        params["v_offset"] = o
        steppi.add_run_to_db(params)
        
#%%

#run them 
run_all_sims()
#do the evaluations
run_all_evals()

#%%
#gather additional trace data for offsets

select_params = {n:None for n in steppi.param_names}

temp_range = steppi.get_param_range("temp")
offset_range = steppi.get_param_range("v_offset")
u_hf_range = steppi.get_param_range("u_hf")
r_load_range = steppi.get_param_range("r_load")

res2_o1 = []
select_params["temp"] = 25
select_params["v_offset"] = 0.05
select_params["r_load"] = 10e3
for u in u_hf_range:
    select_params["u_hf"] = u
    run_id = steppi.get_ids_where(select_params)
    if run_id:
        res2_o1.append(steppi.get_eval_result(run_id[0], "v_out_pwr"))
         
res2_o2 = []
select_params["temp"] = 25
select_params["v_offset"] = 0.1
select_params["r_load"] = 10e3
for u in u_hf_range:
    select_params["u_hf"] = u
    run_id = steppi.get_ids_where(select_params)
    if run_id:
        res2_o2.append(steppi.get_eval_result(run_id[0], "v_out_pwr"))
        
#%%
#plot i/o characteristic with offsets

plt.rcParams['figure.figsize'] = [12, 5]
fig, ax = plt.subplots(1,2)


ax[0].plot(u_hf_range, res2, marker='.', color="grey", label="no offset")
ax[0].plot(u_hf_range, res2_o1, marker='.', color="tab:orange", label="50 mV offset")
ax[0].plot(u_hf_range, res2_o2, marker='.', color="tab:green", label="100 mV offset")


ax[1].semilogy(input_levels_dBm, res2, marker='.', color="grey")
ax[1].semilogy(input_levels_dBm, res2_o1, marker='.', color="tab:orange")
ax[1].semilogy(input_levels_dBm, res2_o2, marker='.', color="tab:green")

ax[0].set_xlabel("Input Voltage [V]")
ax[1].set_xlabel("Input Power [dBm]")

ax[0].set_ylabel("Output Voltage [V]")
ax[1].set_ylabel("Output Voltage [V]")

ax[0].legend(loc="upper left")

fig.tight_layout(pad=1.15)

#plt.show()
plt.savefig('pictures/vout_curves_w_offset.png', transparent=False, dpi=100)
plt.close()
